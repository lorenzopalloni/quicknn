jupyter==1.0.0
numpy==1.14.5
pandas==0.23.4
path.py==11.0.1
scikit-learn==0.19.2
sklearn==0.0
tensorflow==1.11.0
tensorboard==1.11.0
